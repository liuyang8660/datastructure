package cn.edu.bupt.sdmda.main;

public class SimpleFileSystem {
	private FATNode[] fat;
	private ClusterNode[] cluster;
	private int cluster_size;
	private int cluster_num;

	public SimpleFileSystem(int size, int num) {
		cluster_size = size;
		cluster_num = num;
		fat = new FATNode[cluster_num];
		cluster = new ClusterNode[cluster_num];
		for (int i = 0; i < cluster_num; ++i) {
			fat[i] = new FATNode();
			cluster[i] = new ClusterNode(cluster_size);
		}

	}

	// Just for the convenience of testing
	public SimpleFileSystem() {
		this(32, 50);
	}

	public boolean putFile(MyFile file) {
		// TODO write your code
		return true;
	}

	public boolean deleteFile(String filename) {
		// write your code
		return true;
	}

	// a function to show the information of file
	public void showInfo() {
		for (FATNode hn : fat) {
			if (!hn.filename.equals("")) {
				System.out.println("File:" + hn.filename);
				int cur = hn.startIdx;
				while (cur != -1) {
					System.out.print("\t" + cur + " ");
					cur = cluster[cur].next;
				}
				System.out.println();
			}
		}
	}
}

class FATNode {
	String filename;
	int startIdx;

	public FATNode() {
		filename = "";
		startIdx = -1;
	}
}

class ClusterNode {
	boolean flag;
	byte[] data;
	int next;

	public ClusterNode(int size) {
		flag = false;
		data = new byte[size];
		next = -1;
	}
}

class MyFile {
	String filename;
	int size;

	public MyFile(String name, int size) {
		this.filename = name;
		this.size = size;
	}
}
